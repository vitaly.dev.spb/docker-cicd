FROM python:3-alpine
ENV PYTHONUNBUFFERED 1
USER root

WORKDIR app
COPY docker-cicd/. .
RUN mkdir db
RUN apk add curl
RUN pip install -r requirements.txt
RUN chmod +x start.sh

EXPOSE 8000

#CMD /app/start.sh
CMD sh
